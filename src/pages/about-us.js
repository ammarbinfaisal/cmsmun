import React from "react";
import Helmet from "react-helmet";

import "../sass/page.sass";

const About = () => (
	<div id="about-us" className="plain-page">
		<Helmet>
			<title>About Us | CMS ALIGANJ</title>
		</Helmet>
		<h1>Haven't made this page yet :D</h1>
		<p>Comeback later</p>
	</div>
);

export default About;
